﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Media;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace StopCuttingTrees
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        Random r = new Random();
        int score = 0;
        int total_shots = 0;
        int miss_shot = 0;



        void fn_shot()
        {
            score++;
            label1.Text = "Score=" + score;
            total_shots++;
            label3.Text = "Total Shots=" + total_shots;
            //shot_voice();
        }
        /*void shot_voice()
        {
            SoundPlayer simpleSound = new SoundPlayer(@"D:\Sve sa destkopa\Zr fax\Racunarska Grafika 2\RG2\Seminarski\zvuk puske\shot.wav");
            simpleSound.Play();

        }*/
        void fn_miss_shot()
        {
            //shot_voice();

            miss_shot++;
            label2.Text = "Miss Shots=" + miss_shot;
            total_shots++;
            label3.Text = "Total Shots=" + total_shots;
            
        }
        void reset()
        {
            score = 0;
            miss_shot = 0;
            total_shots = 0;
            label1.Text = "Score= " + score;
            label2.Text = "Miss Shots= " + miss_shot;
            label3.Text = "Total Shots= " + total_shots;
            timer1.Start();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {

            int x, y;
            x = r.Next(0, 600);
            y = r.Next(300, 460);
            pictureBox1.Location = new Point(x, y);   
            if (miss_shot >= 10)
            {
                timer1.Stop();
                label1.Text = "Score= " + score;
                label2.Text = "";

                if (MessageBox.Show("New game?", "Game Over", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    reset();
                }
                else
                {
                    this.Close();
                }
            }
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            fn_shot();//executovanje funkcije fn_shot
        }

        private void Form1_MouseClick(object sender, MouseEventArgs e)
        {
            fn_miss_shot();
        }
        //reset button
        private void button2_Click(object sender, EventArgs e)
        {
            reset();
        }
        //exit button
        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        //easy button
        private void button3_Click(object sender, EventArgs e)
        {
            reset();
            timer1.Interval = 800;
        }
        //hard button
        private void button5_Click(object sender, EventArgs e)
        {
            reset();
            timer1.Interval = 300;
        }
        //medium button
        private void button4_Click(object sender, EventArgs e)
        {
            reset();
            timer1.Interval = 600;
        }
    }
}
